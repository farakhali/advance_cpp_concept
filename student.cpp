#include "student.h"
using namespace ns;

void geek::display()
{
    float number=0;
    cout << "Enter number to find its square : ";
    cin >> number;

    float result = number * number;

    cout<< "Square of number " << number << " is " << result << "\n";
}